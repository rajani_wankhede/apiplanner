FROM node:latest
# Create app directory
RUN mkdir /app
WORKDIR /app

# Install app dependencies
COPY package.json /app
RUN npm install

# Bundle app source
COPY . /app
EXPOSE 5000
CMD [ "node", "index" ]
