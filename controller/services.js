var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var ObjectId = require('mongodb').ObjectID;
var projectServiceValidator = require('../validator/services.js');
var guid = require("guid");


module.exports = function(db) {
    return {
        //add service to project
        addProjectServie: function(req, res) {
            req.checkBody(projectServiceValidator);
            if (!req.validateAndRespond()) return;
            req.checkParams('uid', 'uid must exist').notEmpty();
            req.checkParams('pid', 'pid must exist').notEmpty();
            var pid = req.params.pid;
            var uid = req.params.uid;
            var serviceData = req.body.service;
            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    if (uid != result.ownerId) {
                        return res.send({
                            err: true,
                            error: "provide user is not project owner he can't change details"
                        });
                    } else {
                        serviceData.id = guid.raw();
                        serviceData.projectId = result._id;
                        db.collection('project').update({ _id: new ObjectId(pid) }, { $push: { services: serviceData } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: "project not found "
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: "new service added to project"
                                });
                            }
                        });
                    }
                }
            });
        },
        getProjectAllServies: function(req, res) {
            req.checkParams('pid', 'pid must exist').notEmpty();
            var pid = req.params.pid;
            db.collection('project').findOne({ _id: new ObjectId(pid) }, { "services": 1, _id: 0 }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {
                    return res.send({
                        err: false,
                        result: result
                    });
                }
            });
        },
        getProjectSingleServies: function(req, res) {
            req.checkParams('sid', 'sid must exist').notEmpty();
            var sid = req.params.sid;
            db.collection('project').findOne({ "services.id": sid }, { "services.$": 1 }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "service not found with given id"
                    });
                } else {
                    return res.send({
                        err: false,
                        result: result
                    });
                }
            });
        },
        updateProjectService: function(req, res) {
            req.checkBody(projectServiceValidator);
            if (!req.validateAndRespond()) return;
            var pid = req.params.pid;
            var sid = req.params.sid;
            var serviceData = req.body.service;
            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, project) {
                if (project === null) {
                    return res.send({
                        err: true,
                        error: "No project found with given Unique Id"
                    });
                } else {
                    db.collection('project').update({ _id: new ObjectId(pid) }, { $pull: { services: { id: sid } } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: "project service not found"
                            });
                        } else {
                            serviceData.id = guid.raw();
                            serviceData.projectId = project._id;
                            console.log(serviceData.id + ' ' + serviceData.projectId);
                            console.log(result);
                            var dateNow = new Date();
                            serviceData.updatedon = dateNow;
                            db.collection('project').update({ _id: new ObjectId(pid) }, { $push: { services: serviceData } }, function(err, result) {
                                if (err) {
                                    return res.send({
                                        err: true,
                                        error: err
                                    });
                                } else {
                                    return res.send({
                                        err: false,
                                        error: "services updated successfully"
                                    });
                                }
                            });
                        }
                    });
                }
            });
        },
        deleteProjectService: function(req, res) {
            req.checkParams('pid', 'pid must exist').notEmpty();
            req.checkParams('sid', 'sid must exist').notEmpty();
            var pid = req.params.pid;
            var sid = req.params.sid;


            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, project) {
                if (project === null) {
                    return res.send({
                        err: true,
                        error: "No project found with given Unique Id"
                    });
                } else {
                    console.log("pid" + pid);
                    console.log("sid" + sid);

                    db.collection('project').update({ _id: new ObjectId(pid) }, { $pull: { services: { id: sid } } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: "No service found, not deleted"
                            });
                        } else {
                            return res.send({
                                err: false,
                                error: "services deleted successfully done"
                            });
                        }
                    });
                }
            });
        },
        getCountOfServices: function(req, res) {

        }
    };

};