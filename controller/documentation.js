var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var ObjectId = require('mongodb').ObjectID;
var documentationValidator = require('../validator/documentation.js');
var guid = require("guid");


module.exports = function(db) {
    return {
        addDocumentation: function(req, res) {
            // req.checkBody(documentationValidator);
            // if (!req.validateAndRespond()) return;
            var id = req.params.sid;
            var documentationData = req.body.documentation;

            db.collection('project').findOne({ "services.id": id }, { "services.$": 1 }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "service not found"
                    });
                } else {
                    console.log(result);
                    documentationData.serviceName = result.services[0].name;
                    documentationData.serviceId = result.services[0].id;
                    documentationData.serviceDescription = result.services[0].description;

                    db.collection('documentation').insertOne(documentationData, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            res.send({
                                err: false,
                                result: "Api documentaion added successfully"
                            });
                        }
                    });
                }
            });

        },
        getDocumentation: function(req, res) {
            req.checkParams('did', 'did must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var did = req.params.did;
            db.collection('documentation').findOne({ _id: new ObjectId(did) }, { password: 0, _id: 0 }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "documentation not found with given id"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            user: result
                        }
                    });
                }
            });
        },
        Updateocumentation: function(req, res) {
            req.checkParams('did', 'did must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var documentationData = req.body.documentation;

            var did = req.params.did;

            db.collection('documentation').findOne({ _id: new ObjectId(did) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    var dateNow = new Date();
                    documentationData.updatedon = dateNow;
                    db.collection('documentation').update({ _id: new ObjectId(did) }, { $set: documentationData }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: "documentation not foun with given id"
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: "documentation updated successfully"
                            });
                        }
                    });
                }
            });



        },
        deleteDocument: function(req, res) {
            var did = req.params.did;
            db.collection('documentation').remove({ _id: new ObjectId(did) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "documentation not foun with given id"
                    });
                } else {
                    return res.send({
                        err: false,
                        result: "documentation deleted successfully"
                    });
                }
            });

        },
        //get all document of service
        getAlldocumentOfService: function(req, res) {
            sid = req.params.sid;
            db.collection('documentation').find({ "serviceId": sid }).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "can't get all projects"
                    });
                } else {
                    res.json({
                        err: false,
                        result: result
                    });
                }
            });
        },
        getCountOfDocumentation: function(req, res) {
            db.collection('documentation').find({}).count(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "can't get count"
                    });
                } else {
                    console.log(result)
                    res.send({
                        err: false,
                        result: {
                            documentaion: result
                        }
                    });
                }
            });
        }
    };
};