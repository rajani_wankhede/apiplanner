var generators = require("../helper/generators.js");
var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var password = require("../helper/password.js");
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../helper/sendMail.js');
var send_sms = require('../helper/send_sms.js');
var projectValidator = require('../validator/project.js');
var guid = require("guid");

//html to pdf conver requires following
var fs = require('fs');
var pdf = require('html-pdf');

//with hbs-pdf
let pdf1 = require('handlebars-pdf');
const path = require('path');

var contents = fs.readFileSync('./views/report.hbs', 'utf8');
var _path = "./outs/test-" + Math.random() + ".pdf";


module.exports = function(db) {
    return {
        addProject: function(req, res) {
            req.checkBody(projectValidator);
            if (!req.validateAndRespond()) return;
            var projectData = req.body.project;
            var uid = req.params.uid;

            try {
                projectData.projectCode = generators.UniqueIdGenerator(projectData.name, projectData.isPrivate)
            } catch (e) {
                console.log(e);
                return res.send({
                    err: true,
                    error: "name and isPrivate fiels  must be valid"
                });
            }

            db.collection('user_master').findOne({ _id: new ObjectId(uid) }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "user not found with given sid"
                    });

                } else {
                    console.log(result)
                    projectData.ownerId = result._id;
                    console.log(result.userrole);
                    if (result.userrole === "read") {
                        res.send({
                            err: false,
                            result: "provided user have read acces permissions"
                        });

                    } else {
                        db.collection('project').insertOne(projectData, function(err, result) {
                            console.log(err);
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                res.send({
                                    err: false,
                                    result: "project added successsfully"
                                });
                            }
                        });
                    }
                }
            });

        },
        getSingleProject: function(req, res) {
            req.checkParams('pid', 'pid must exist').notEmpty();
            var pid = req.params.pid;
            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    return res.send({
                        err: false,
                        result: result
                    });
                }

            });
        },
        getAllProject: function(req, res) {
            db.collection('project').find({}).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "can't get all projects"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            project: result
                        }
                    });
                }
            });
        },
        //update project by only project ownerId
        updateProject: function(req, res) {
            req.checkParams('uid', 'uid must exist').notEmpty();
            req.checkParams('pid', 'pid must exist').notEmpty();
            var pid = req.params.pid;
            var uid = req.params.uid;
            projectData = req.body.project;

            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    if (uid != result.ownerId) {
                        return res.send({
                            err: true,
                            error: "provide user is not project owner he can't change details"
                        });
                    } else {
                        var dateNow = new Date();
                        projectData.updatedon = dateNow;
                        db.collection('project').update({ _id: new ObjectId(pid) }, { $set: projectData }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: "project not foun with given id"
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: "project updated successfully"
                                });
                            }
                        });
                    }
                }
            });
        },
        //delete project only by project owner
        deleteProject: function(req, res) {
            var pid = req.params.pid;
            var uid = req.params.uid;
            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    console.log(result);
                    console.log(result.ownerId)
                    if (uid != result.ownerId) {
                        return res.send({
                            err: true,
                            error: "provide user is not project owner he can't delete project"
                        });
                    } else {
                        db.collection('project').remove({ _id: new ObjectId(pid) }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: "project not foun with given id"
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: "project deleted successfully"
                                });
                            }
                        });
                    }
                }
            });
        },
        //make active status of project by project owner
        makeActive: function(req, res) {
            req.checkParams('uid', 'uid must exist').notEmpty();
            req.checkParams('pid', 'pid must exist').notEmpty();
            var pid = req.params.pid;
            var uid = req.params.uid;

            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    if (uid != result.ownerId) {
                        return res.send({
                            err: true,
                            error: "provide user is not project owner he can't delete project"
                        });
                    } else {
                        db.collection('project').update({ _id: new ObjectId(pid) }, { $set: { "status": "active" } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: result
                                });
                            }
                        });
                    }
                }
            });
        },
        makeInActive: function(req, res) {
            req.checkParams('uid', 'uid must exist').notEmpty();
            req.checkParams('pid', 'pid must exist').notEmpty();
            var pid = req.params.pid;
            var uid = req.params.uid;

            db.collection('project').findOne({ _id: new ObjectId(pid) }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "project not found with given id"
                    });
                } else {
                    if (uid != result.ownerId) {
                        return res.send({
                            err: true,
                            error: "provide user is not project owner he can't delete project"
                        });
                    } else {
                        db.collection('project').update({ _id: new ObjectId(pid) }, { $set: { "status": "inactive" } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: result
                                });
                            }
                        });
                    }
                }
            });
        },
        getcountOfProject: function(req, res) {
            db.collection('project').find({}).count(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "can't get all projects"
                    });
                } else {
                    console.log(result)
                    res.send({
                        err: false,
                        result: {
                            project: result
                        }
                    });
                }
            });
        },
        getallservicesofproject: function(req, res) {
            var pid = req.params.pid;
            db.collection('project').findOne({ _id: new ObjectId(pid) }, { "services": 1, "name": 1, _id: 0 }, function(err, service) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {

                    var services = [];
                    services = service.services;
                    //  console.log(services);
                    var sid = services.map(function(a) { return a.id; });
                    // console.log(sid);
                    db.collection('documentation').find({ "serviceId": { $in: sid } }).toArray(function(err, result) {
                        if (result.length === 0) {
                            return res.send({
                                err: true,
                                error: "can't get documentation, or there is no documentaion for this project available"
                            });
                        } else {
                            debugger
                            console.log(result);
                            projectname = service.name;
                            // res.render('report.html', { result: result });

                            let document = {
                                template: contents,
                                context: { result: result, name: projectname },
                                path: _path
                            }

                            pdf1.create(document)
                                .then(ress => {
                                    console.log(ress)
                                    var data = fs.readFileSync(_path);
                                    res.contentType("application/pdf");
                                    res.send(data);
                                })
                                .catch(error => {
                                    console.error(error)
                                });
                        }
                    });
                }
            });
        },
        //convert html to pdf
        htmltopdf: function(req, resp) {
            var data = req.body.data;
            data = data.replace(/(\r\n|\n|\r)/gm, "");
            console.log(data + "here.................");
            var stdata = JSON.stringify(data);
            //var dt = toString(data);
            // var html = fs.readFileSync('./htmlfile/demo.html', 'utf8');
            var options = { format: 'Letter' };
            pdf.create(stdata, options).toFile('./pdffile/businesscard.pdf', function(err, res) {
                if (err) {
                    return console.log(err);
                } else {
                    return resp.send({
                        err: false,
                        result: "project added successsfully"
                    });
                }
            });
        },
        //convert with hbs
        hbstopdf: function(req, res) {
            let document = {
                template: contents,
                context: data,
                path: _path
            }
            debugger
            pdf1.create(document)
                .then(ress => {
                    console.log(ress)
                    var data = fs.readFileSync(_path);
                    res.contentType("application/pdf");
                    res.send(data);
                })
                .catch(error => {
                    console.error(error)
                });
        }
    };
};