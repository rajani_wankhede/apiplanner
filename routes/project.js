module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'user_master');

    //addded project routes 
    //added user token authentication
    //for testing removed authentication i.e authentication.ensureAuthentication
    var project = require('../controller/project.js')(db);
    app.post('/user/addProject/:uid', authentication.ensureAuthentication, project.addProject);
    app.get('/user/getSingleproject/:pid', project.getSingleProject);
    app.get('/user/getAllProject', project.getAllProject);
    app.get('/getcountOfProject', project.getcountOfProject);
    //update, delete , make active/inactive only by project owner
    app.put('/owner/updateProject/:uid/:pid', authentication.ensureAuthentication, project.updateProject);
    app.delete('/owner/deleteProject/:pid/:uid', authentication.ensureAuthentication, project.deleteProject);
    app.put('/owner/makeActive/:uid/:pid', authentication.ensureAuthentication, project.makeActive);
    app.put('/owner/makeInActive/:uid/:pid', authentication.ensureAuthentication, project.makeInActive);
    app.get('/getallservicesofproject/:pid', project.getallservicesofproject);
    app.post('/htmltopdf', project.htmltopdf);
    app.post('/hbstopdf', project.hbstopdf);
};