module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'user_master');

    //add,update,delete services in array to project, only by user with write permissions
    //added user token authentication
    var services = require('../controller/services.js')(db);
    app.post('/user/addProjectServie/:uid/:pid', authentication.ensureAuthentication, services.addProjectServie);
    app.get('/user/getProjectAllServies/:pid', services.getProjectAllServies);
    app.get('/getCountOfServices', services.getCountOfServices);
    app.get('/user/getProjectSingleServies/:sid', services.getProjectSingleServies);

    app.delete('/user/deleteProjectService/:pid/:sid', authentication.ensureAuthentication, services.deleteProjectService)
    app.put('/user/updateProjectService/:pid/:sid', authentication.ensureAuthentication, services.updateProjectService);
    // app.delete('user/deleteProjectService/:uid/:sid', services.deleteProjectService)
};