module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'user');
    var documentation = require('../controller/documentation.js')(db);

    //documentation routes
    app.post('/addDocumentation/:sid', documentation.addDocumentation);
    app.get('/getDocumentation/:did', documentation.getDocumentation);
    app.get('/getAlldocumentOfService/:sid', documentation.getAlldocumentOfService);
    app.get('/getCountOfDocumentation', documentation.getCountOfDocumentation);
    app.put('/UpDdateocumentation/:did', documentation.Updateocumentation);
    app.delete('/deleteDocument/:did', documentation.deleteDocument);
};