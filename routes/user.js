module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'user');
    var user = require('../controller/user.js')(db);
    //user routes
    app.post('/adminuser', user.addAdmin);
    app.post('/addUser', user.addUser);
    app.put('/updateuser/:id', user.updateUser);
    app.get('/getuser/:id', user.getUser);
    app.get('/getAllUser', user.getAllUser);
    app.post('/userLogin', user.userLogin);
    app.post('/user/forgotpassword/email', user.forgotpassword);
    app.post('/user/:id/logout', user.userlogout);
    app.post('/user/:id/forgotpassword/sms', user.forgotpasswordSMS);
    app.post('/user/:id/Changepassword', user.ChangePassword);
    app.post('/user/:id/activate', user.userActivate);
    app.post('/user/:id/inactive', user.inactiveAccount);
    app.delete('/deleteUser/:id', user.deleteUser);


};