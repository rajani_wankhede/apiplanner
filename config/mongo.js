var MongoClient = require("mongodb").MongoClient;
var _ = require("lodash");

var assert = require("assert");
//var url = 'mongodb://db:27017/genral-api-kong-admin-docker';
//use ://db when using docker-compose ..use ://localhost for local ..use ://mongo when use same mongo image
//var url = 'mongodb://localhost:27017/api_planner_DB';
var url = 'mongodb://rajani:rajani123@ds143559.mlab.com:43559/apidocs'
module.exports = function(callback) {
    console.log("here");
    MongoClient.connect(url, function(err, db) {
        if (err) return callback(err, db);
        else {
            db.collection('admin').createIndex({
                email: 1
            }, { unique: true }, function(err, result) {
                if (err) return callback(err, result);
                else {
                    db.collection('admin').createIndex({
                        mobile: 1
                    }, { unique: true }, function(err, result) {
                        if (err) return callback(err, result);
                        else {
                            callback(err, db);
                        }
                    });
                }
            });
        };
    });
};