module.exports = {
    "documentation.apiType": { isString: true, notEmpty: false },
    "documentation.url": { isString: true, notEmpty: false },

    //add url parameter array
    "documentation.headerParameter.key": { isString: true, notEmpty: false },
    "documentation.headerParameter.value": { isString: true, notEmpty: false },

    //add parameter array
    "documentation.params.field": { isString: true, notEmpty: false },
    "documentation.params.type": { isString: true, notEmpty: false },
    "documentation.params.description": { isString: true, notEmpty: false },

    //sucees and errror examples
    "documentation.exampleUsage": { isString: true, notEmpty: false },
    "documentation.successResponse": { isString: true, notEmpty: false },
    "documentation.errorResponse": { isString: true, notEmpty: false },
};