module.exports = {
    "user.username": { isString: true, notEmpty: false },
    "user.password": { notEmpty: true, isString: true },
    "user.firstName": { isString: true, notEmpty: false },
    "user.lastName": { isString: true, notEmpty: false },
    "user.email": { notEmpty: true, isEmail: true },
    "user.mobile": { notEmpty: true, isString: true },
    "user.usertype": { isString: true, notEmpty: false },
    "user.userrole": { isString: true, notEmpty: false },
    "user.status": { isString: true, notEmpty: false }
};