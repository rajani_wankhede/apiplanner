module.exports = {
    "project.name": { isString: true, notEmpty: false },
    "project.isPrivate": { isString: true, notEmpty: false },
    "project.purpose": { isString: true, notEmpty: false },
    "project.status": { isString: true, notEmpty: false },
    "project.authenticationDescription": { isString: true, notEmpty: false }
};