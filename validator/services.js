module.exports = {
    "service.name": { isString: true, notEmpty: false },
    "service.description": { isString: true, notEmpty: false },
    "service.status": { isString: true, notEmpty: false }
};