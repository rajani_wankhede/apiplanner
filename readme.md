README FILE 


#following steps used when running app with local
STEPS TO RUN API:
1)Pull the code
2)go to the apiplanner folder(base folder)
3)at apiplanner hit "npm install" it will add npm packages for api 
4)node index.js (run apis)


STEPS TO RUN PROJECT:
1)pull the code
2)go to the API-PLANNER-APP Folder
3)at API-PLANNER-APP hit "npm install" it will add npm packages 
4)to run UI app hit "npm run dev" 




#following steps used when running app with docker containers
#STEPS to run containers
1)go to folder path apiplanner and hit "sh ./start.sh"
2)it will run three docker containers(ui.api and mongo) they are link with each other

Now,
we setup golbal.js(for provide global ip's) file inside api-planner-app/static/js/global.js open this file.

3)hit on terminal "docker inspect appapi" copy "IPAddree"(like 172.17.0.4) 
 and set up on globalLocalHost="http://172.17.0.4:5000;" variable
 
4)on terminal hit "docker inspect appui" copy IPAddress and go to browser and hit
"IpAddress:8080" it will run the app



 