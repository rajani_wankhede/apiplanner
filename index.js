var express = require("express");
var app = express();
var exphbs = require('express-handlebars');

require("./config/express.js")(app);
var cors = require('cors');
var port = process.env.PORT || 5000;


var hbs = require("hbs").__express;


app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.engine('html', hbs);

app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

require("./config/mongo.js")(function(err, db) {
    if (err) throw err;
    require("./routes/project.js")(db, app);
    require("./routes/user.js")(db, app);
    require("./routes/services.js")(db, app);
    require("./routes/documentation.js")(db, app);
    app.listen(port);
    console.log("Application listening on port" + port);
});

module.exports = app;